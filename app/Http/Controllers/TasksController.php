<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TasksController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$tasks = Task::all();
        $tasks = Task::orderBy('title' , 'desc')->paginate(5);
        $now = date("Y-m-d", time());  
        return view('pages/tasks/index')->with('tasks' , $tasks)->with('now', $now);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages/tasks/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
            //'date' => 'required'
        ]);
        

        $task = new Task;
        $task->title = $request->input('title');
        $task->body = $request->input('body');
        $task->date = $request->input('date');
        $task->user_id = auth()->user()->id;
        $task->save();

        return redirect('/tasks')->with('success' , 'Task Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::find($id);
        return view('/pages/tasks/show')->with('task' , $task);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);
        return view('/pages/tasks/edit')->with('task' , $task);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);
        

        $task = Task::find($id);
        if($request->has('completed')) {
            $task->completed = 'ja';
        }
        $task->title = $request->input('title');
        $task->body = $request->input('body');
        $task->date = $request->input('date');
        $task->save();

        return redirect('/tasks')->with('success' , 'Task Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);
        $task->delete();

        return redirect('/tasks')->with('success' , 'Task Deleted');
    }
}
