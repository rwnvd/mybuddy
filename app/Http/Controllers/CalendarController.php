<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Calendar;
use App\Appointment;
 

class CalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $day1 = date("Y-m-d", time());
        $day2 = date("Y-m-d", time() + 86400);
        $day3 = date("Y-m-d", time() + 172800);
        $day4 = date("Y-m-d", time() + 259200);
        $day5 = date("Y-m-d", time() + 345600);
        $day6 = date("Y-m-d", time() + 432000);
        $day7 = date("Y-m-d", time() + 518400);

        $appointmentsDay1 = Appointment::query()->where('date', $day1)->get();
        $appointmentsDay2 = Appointment::query()->where('date', $day2)->get();
        $appointmentsDay3 = Appointment::query()->where('date', $day3)->get();
        $appointmentsDay4 = Appointment::query()->where('date', $day4)->get();
        $appointmentsDay5 = Appointment::query()->where('date', $day5)->get();
        $appointmentsDay6 = Appointment::query()->where('date', $day6)->get();
        $appointmentsDay7 = Appointment::query()->where('date', $day7)->get();

        return view('pages/calendar/index')->with('appointmentsDay1' , $appointmentsDay1)->with('appointmentsDay2' , $appointmentsDay2)->with('appointmentsDay3' , $appointmentsDay3)->with('appointmentsDay4' , $appointmentsDay4)->with('appointmentsDay5' , $appointmentsDay5)->with('appointmentsDay6' , $appointmentsDay6)->with('appointmentsDay7' , $appointmentsDay7);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
