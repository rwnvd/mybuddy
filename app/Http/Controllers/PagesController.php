<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function afspraken()
    {
        return view('pages/afspraken');
    }
}
