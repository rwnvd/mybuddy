<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Appointment;

class AppointmentsController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $appointments = Appointment::orderBy('title' , 'asc')->paginate(5);
        return view('pages/appointments/index')->with('appointments' , $appointments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages/appointments/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'date' => 'required'
        ]);
        

        $appointment = new Appointment;
        $appointment->title = $request->input('title');
        $appointment->body = $request->input('body');
        $appointment->date = $request->input('date');
        $appointment->start_time = $request->input('start_time');
        $appointment->end_time = $request->input('end_time');
        $appointment->user_id = auth()->user()->id;
        $appointment->save();

        return redirect('/appointments')->with('success' , 'Appointment Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $appointment = Appointment::find($id);
        return view('pages/appointments/show')->with('appointment' , $appointment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $appointment = Appointment::find($id);
        return view('/pages/appointments/edit')->with('appointment' , $appointment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'date' => 'required'
        ]);
        

        $appointment = Appointment::find($id);
        $appointment->title = $request->input('title');
        $appointment->body = $request->input('body');
        $appointment->date = $request->input('date');
        $appointment->start_time = $request->input('start_time');
        $appointment->end_time = $request->input('end_time');
        $appointment->save();

        return redirect('/appointments')->with('success' , 'Appointment Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $appointment = Appointment::find($id);
        $appointment->delete();

        return redirect('/appointments')->with('success' , 'Appointment Deleted');
    }
}
