@extends('layouts.app')
@php
$day1 = date("Y-m-d", time());
$day2 = date("Y-m-d", time() + 86400);
$day3 = date("Y-m-d", time() + 172800);
$day4 = date("Y-m-d", time() + 259200);
$day5 = date("Y-m-d", time() + 345600);
$day6 = date("Y-m-d", time() + 432000);
$day7 = date("Y-m-d", time() + 518400);
@endphp

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Kalender</div> 
                        <div class="card-header">@php echo $day1 @endphp</div>
                        @foreach ($appointmentsDay1 as $appointment)
                            <div class="card-body">
                                <p>{{$appointment->title}} &nbsp &nbsp &nbsp &nbsp &nbsp  {{$appointment->start_time}} &nbsp - &nbsp {{$appointment->end_time}}</p>
                            </div>
                        @endforeach
                        <div class="card-header">@php echo $day2 @endphp</div>
                        @foreach ($appointmentsDay2 as $appointment2)
                            <div class="card-body">
                                <p>{{$appointment2->title}} &nbsp &nbsp &nbsp &nbsp &nbsp  {{$appointment2->start_time}} &nbsp - &nbsp {{$appointment2->end_time}}</p>
                            </div>
                        @endforeach
                        <div class="card-header">@php echo $day3 @endphp</div>
                        @foreach ($appointmentsDay3 as $appointment3)
                            <div class="card-body">
                                <p>{{$appointment3->title}} &nbsp &nbsp &nbsp &nbsp &nbsp  {{$appointment3->start_time}} &nbsp - &nbsp {{$appointment3->end_time}}</p>
                            </div>
                        @endforeach
                        <div class="card-header">@php echo $day4 @endphp</div>
                        @foreach ($appointmentsDay4 as $appointment4)
                            <div class="card-body">
                                <p>{{$appointment4->title}} &nbsp &nbsp &nbsp &nbsp &nbsp  {{$appointment4->start_time}} &nbsp - &nbsp {{$appointment4->end_time}}</p>
                            </div>
                        @endforeach
                        <div class="card-header">@php echo $day5 @endphp</div>
                        @foreach ($appointmentsDay5 as $appointment5)
                            <div class="card-body">
                                <p>{{$appointment5->title}} &nbsp &nbsp &nbsp &nbsp &nbsp  {{$appointment5->start_time}} &nbsp - &nbsp {{$appointment5->end_time}}</p>
                            </div>
                        @endforeach
                        <div class="card-header">@php echo $day6 @endphp</div>
                        @foreach ($appointmentsDay6 as $appointment6)
                            <div class="card-body">
                                <p>{{$appointment6->title}} &nbsp &nbsp &nbsp &nbsp &nbsp  {{$appointment6->start_time}} &nbsp - &nbsp {{$appointment6->end_time}}</p>
                            </div>
                        @endforeach
                        <div class="card-header">@php echo $day7 @endphp</div>
                        @foreach ($appointmentsDay7 as $appointment7)
                            <div class="card-body">
                                <p>{{$appointment7->title}} &nbsp &nbsp &nbsp &nbsp &nbsp  {{$appointment7->start_time}} &nbsp - &nbsp {{$appointment7->end_time}}</p>
                            </div>
                        @endforeach
                        </div>
            </div>    
        </div>
    </div>
</div>
@endsection