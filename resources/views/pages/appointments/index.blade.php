@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Afspraken</div>
                <div class="card-body">
                    <a href="appointments/create" class="btn btn-primary btn-create">create</a>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(count($appointments) > 0)
                    @foreach($appointments as $appointment)
                    <div class="card dashboard-card">
                        <div class="card-body">
                            <h3><a href="/appointments/{{$appointment->id}}">{{$appointment->title}}</a></h3>
                            <small>Datum: {{$appointment->date}}</small><br>
                            @if(is_null($appointment->start_time))
                                <small>Van: n.v.t.</small><br>
                            @else
                                <small>Van: {{$appointment->start_time}}</small><br>
                            @endif
                            @if(is_null($appointment->end_time))
                            <small>Tot: n.v.t.</small><br>
                            @else
                            <small>Tot: {{$appointment->end_time}}</small><br>
                            @endif
                        </div>
                    </div>
                    @endforeach
                    {{$appointments->links()}}
                @else
                  <p> Geen afspraken gevonden</p>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection