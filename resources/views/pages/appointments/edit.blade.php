@extends('layouts.app')

@section('content')
<div class="container">
        <button class="btn btn-primary btn-back2" onclick="goBack()">Terug</button>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">update een Afspraak</div>
                    <div class="card-body">
                        {!! Form::open(['action' => ['AppointmentsController@update' , $appointment->id] , 'method' => 'POST']) !!}
                            <div class="form-group">
                                {{Form::label('title', 'Titel')}}
                                {{Form::text('title' , $appointment->title, ['class' => 'form-control' , 'placeholder' => 'Titel'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('body', 'Body')}}
                                {{Form::textarea('body' , $appointment->body, ['class' => 'form-control' , 'placeholder' => 'Body'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('date' , 'Date')}}
                                {{Form::date('date' , $appointment->date , ['class="form-control"'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('start_date' , 'Begin tijd')}}
                                {{Form::time('start_time' , $appointment->start_time , ['class="form-control"'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('end_date' , 'Eind tijd')}}
                                {{Form::time('end_time' , $appointment->end_time , ['class="form-control"'])}}
                            </div>
                            {{Form::hidden('_method' , 'PUT')}}
                            {{Form::submit('Submit' , ['class' => 'btn btn-primary'])}}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>  
function goBack() {
    window.history.back();
}
</script>
@endsection