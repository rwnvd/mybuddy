@extends('layouts.app')

@section('content')
<a href="/appointments" class="btn btn-primary btn-back">Terug</a>
    <div class="card dashboard-card card-show">
        <div class="card-body">
            <h1>{{$appointment->title}}</h1>
            <small>Datum: {{$appointment->date}}</small><br>
            @if(is_null($appointment->start_time))
            <small>Van: n.v.t.</small><br>
            @else
                <small>Van: {{$appointment->start_time}}</small><br>
            @endif
            @if(is_null($appointment->end_time))
            <small>Tot: n.v.t.</small><br>
            @else
            <small>Tot: {{$appointment->end_time}}</small><br>
            @endif
            <div>
                {{$appointment->body}}
            </div>
            <a href="/appointments/{{$appointment->id}}/edit" class="btn btn-primary btn-circle"><i class="fas fa-cog"></i><a>
            {!!Form::open(['action' => ['AppointmentsController@destroy' , $appointment->id] , 'method' => 'POST'])!!}
                {{Form::hidden('_method' , 'DELETE')}}
                {{Form::submit('' , ['class' => 'btn btn-danger btn-circle'])}}
            {!!Form::close()!!}
        </div>
    </div>
@endsection