@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Taken</div>
                <div class="card-body">
                    <a href="tasks/create" class="btn btn-primary btn-create">create</a>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if(count($tasks) > 0)
                        @foreach($tasks as $task)
                        <div class="card dashboard-card">
                            {{--<h3>{{$task->date}}</h3>--}}                                               
                                <div class="card-body">
                                    @if($task->completed === "ja")
                                        <h3><a href="/tasks/{{$task->id}}" style="color:green;">{{$task->title}}</a></h3>
                                    @elseif(isset($task->date))
                                        @if($task->date < $now)
                                            <h3><a href="/tasks/{{$task->id}}" style="color:red;">{{$task->title}}</a></h3>
                                        @else 
                                            <h3><a href="/tasks/{{$task->id}}">{{$task->title}}</a></h3>
                                        @endif
                                    @else
                                        <h3><a href="/tasks/{{$task->id}}">{{$task->title}}</a></h3>
                                    @endif
                                    @if(is_null($task->date))
                                        <small>datum: n.v.t.</small>
                                    @else 
                                        <small>datum: {{$task->date}}</small>
                                    @endif
                                        <br><small>afgerond: {{$task->completed}}</small>
                                </div>
                            </div>
                        @endforeach
                        {{$tasks->links()}}
                    @else
                      <p> Geen taken gevonden</p>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection