@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Maak een Taak</div>
                    <div class="card-body">
                        {!! Form::open(['action' => 'TasksController@store' , 'method' => 'POST']) !!}
                            <div class="form-group">
                                {{Form::label('title', 'Titel')}}
                                {{Form::text('title' , '', ['class' => 'form-control' , 'placeholder' => 'Titel'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('body', 'Body')}}
                                {{Form::textarea('body' , '', ['class' => 'form-control' , 'placeholder' => 'Body'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('date' , 'Date')}}
                                {{Form::date('date' , '' , ['class="form-control"'])}}
                            </div>
                            {{Form::submit('Submit' , ['class' => 'btn btn-primary'])}}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection