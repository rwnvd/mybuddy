@extends('layouts.app')

@section('content')
<div class="container">
    <button class="btn btn-primary btn-back2" onclick="goBack()">Terug</button>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Task</div>
                    <div class="card-body">
                        {!! Form::open(['action' => ['TasksController@update', $task->id] , 'method' => 'POST']) !!}
                            <div class="form-group">
                                {{Form::label('title', 'Titel')}}
                                {{Form::text('title' , $task->title, ['class' => 'form-control' , 'placeholder' => 'Titel'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('body', 'Body')}}
                                {{Form::textarea('body' , $task->body , ['class' => 'form-control' , 'placeholder' => 'Body'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('date' , 'Date')}}
                                {{Form::date('date' , $task->date , ['class="form-control"'])}}
                            </div>
                                 <div class="form-group">
                                {{Form::label('completed' , 'Completed:')}}
                                {{Form::checkbox('completed', '1' )}}
                            </div>
                            {{Form::hidden('_method' , 'PUT')}}
                            {{Form::submit('Submit' , ['class' => 'btn btn-primary'])}}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>  
function goBack() {
    window.history.back();
}
</script>
@endsection