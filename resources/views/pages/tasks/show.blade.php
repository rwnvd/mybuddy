@extends('layouts.app')

@section('content')
<a href="/tasks" class="btn btn-primary btn-back">Terug</a>
    <div class="card dashboard-card card-show">
        <div class="card-body">
            <h1>{{$task->title}}</h1>
            @if(is_null($task->date))
            <small>datum: n.v.t.</small>
            @else 
                <small>datum: {{$task->date}}</small>
            @endif
            <div>
                {{$task->body}}
            </div>
        <a href="/tasks/{{$task->id}}/edit" class="btn btn-primary btn-circle"><i class="fas fa-cog"></i><a>

        {!!Form::open(['action' => ['TasksController@destroy' , $task->id] , 'method' => 'POST'])!!}
            {{Form::hidden('_method' , 'DELETE')}}
            {{Form::submit('' , ['class' => 'btn btn-danger btn-circle'])}}
        {!!Form::close()!!}
        </div>
    </div>
@endsection